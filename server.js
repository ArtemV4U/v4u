/**
 * Created by kovalev on 05.09.2016.
 */

var express = require('express');
var app = express();

app.use('/public', express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

var PORT = 8080;

app.listen(PORT);

console.log('Server running on port ', PORT);

