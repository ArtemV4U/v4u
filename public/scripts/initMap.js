/**
 * Created by Stoned on 24.09.2016.
 */

//function init_map()
//{
//    var myOptions = {
//        zoom:17,
//        center:new google.maps.LatLng(55.75578562526788,37.61731609325408),
//        mapTypeId: google.maps.MapTypeId.ROADMAP
//    };
//    map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
//    marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(55.75578562526788,37.61731609325408)});
//    infowindow = new google.maps.InfoWindow({content:'<strong>Vape For U</strong><br>Moscow, Russia<br>'});
//    google.maps.event.addListener(marker, 'click', function(){
//        infowindow.open(map,marker);
//    });
//    infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);

ymaps.ready(init);
var myMap,
    myPlacemark;

function init(){
    myMap = new ymaps.Map("map", {
        center: [55.7687,37.5974],
        zoom: 17
    });

    myPlacemark = new ymaps.Placemark([55.7687,37.5975], {
        hintContent: 'Vape For U',
        balloonContent: 'Столица России'
    });

    myMap.geoObjects.add(myPlacemark);
}
